import json, sqlite3, random, copy, time, string
from jwt import decode, encode
from flask import Flask, request, render_template, g, jsonify, make_response
from flask_cors import CORS, cross_origin

app = Flask(__name__)
app.host = "0.0.0.0"

database = "/home/debian/BLE_API/database.db"
cors = CORS(app, resources=r"/api/*")

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Server features ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

def get_db():
    db = getattr(g, '_database', None)

    if db is None:
        db = g._database = sqlite3.connect(database)
    return db

@app.errorhandler(404)
def not_found(error):
    return jsonify(status=404)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~ Database management ~~~~~~~~~~~~~~~~~~~~~~~~~ #

def init_database(db):
    with app.app_context():
        cursor = db.cursor()

        query = """CREATE TABLE IF NOT EXISTS users (
        user_id INT NOT NULL,
        PRIMARY KEY (user_id));
        """

        cursor.execute(query)
        db.commit()

        query = """CREATE TABLE IF NOT EXISTS records (
        user_id_source INT NOT NULL,
        user_id_target INT NOT NULL,
        timestamp_start INT NOT NULL,
        timestamp_end INT NOT NULL,
        rssi REAL NOT NULL,
        distance REAL NOT NULL,
        nb_detection INT NOT NULL,
        PRIMARY KEY (user_id_source, user_id_target, timestamp_start),
        FOREIGN KEY (user_id_source) REFERENCES users(user_id),
	FOREIGN KEY (user_id_target) REFERENCES users(user_id));
        """

        cursor.execute(query)
        db.commit()

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ API Endpoints ~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

@app.route("/api/id", methods=['GET'])
def get_id():
    with app.app_context():
        db = get_db()
        cursor = db.cursor()

        found_id = False
        while not found_id:
            id_client = random.randint(1,50000)
            cursor.execute("SELECT user_id FROM users WHERE user_id = ?;", (id_client,))
            if cursor.fetchone() is None:
                found_id = True

        cursor.execute("INSERT INTO users VALUES (?);", (id_client,))
        db.commit()

        response = make_response(jsonify({"idClient": id_client}), 200)
        return response

@app.route("/api/ping", methods=['POST'])
def ping():
    with app.app_context():
        response_number = 200

        db = get_db()
        cursor = db.cursor()

        data = request.get_json()
        if data and "idSource" in data and "records" in data:
            for record in data["records"]:
                if len(record) == 6:
                    cursor.execute("SELECT * FROM records WHERE user_id_source = ? and user_id_target = ? and timestamp_start = ?", (data["idSource"], record[0], record[1]))
                    if cursor.fetchone() is None:
                        cursor.execute("INSERT INTO records VALUES (?,?,?,?,?,?,?);", (data["idSource"], record[0], record[1], record[2], record[3], record[4], record[5]))
                        db.commit()

        response = make_response(jsonify({}), response_number)
        return response

if __name__ == "__main__":
    db = sqlite3.connect(database)
    init_database(db)
