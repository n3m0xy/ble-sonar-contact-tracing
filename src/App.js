import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import HomeStack from './navigation/MainNavigator';

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import AppNavigator from './navigation/AppNavigator';

const App = () => {
  // App Navigator no manage start of the app depending of auth
  return <AppNavigator />;
};

export default App;
