import * as React from 'react';
import { View, Text, TextInput, Button, StyleSheet, ImageBackground } from 'react-native';
import Global from '../services/global';

const image = require('../assets/img/background.png')
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  text: {
    color: "white",
    fontSize: 30,
    fontWeight: "bold",
    marginTop: 100,
    textAlign: "center"
  },
  button: {
    paddingVertical: 12,
    paddingHorizontal: 25,
    borderRadius: 25,
    fontSize:20
  },
  textinput: {
    height: 40,
    textAlign: "center"
  }
});


export default class MainScreen extends React.Component {
  constructor() {
      super();
      Global.init(this);
      this.state = { idClient: "Undefined", isStarted: false, scanDuration: Global.scanDuration};
      console.log(this.state);
      this.update = this.update.bind(this);
  }

  update(idClient, isStarted) {
    this.setState({idClient: idClient, isStarted: isStarted});
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground source={image} style={styles.image}>
          <Text style={styles.text}>ID: {this.state.idClient}</Text>
        </ImageBackground>
        <View>
          <TextInput style={styles.textinput} placeholder="Duration of scan" onChangeText={scanDuration => this.setState({scanDuration})} defaultValue={this.state.scanDuration}/>
          <Button color="#201858" title="Flush data" onPress={() => {Global.Flush();}}/>
          <Button color="#841584" style={styles.button} title={this.state.isStarted ? "Stop" : "Start"} onPress={() => {Global.BLEmanager(this.state.scanDuration);}}/>
        </View>
      </View>
    );
  }
}
