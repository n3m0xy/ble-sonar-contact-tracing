'use strict';
import { AppState } from 'react-native';
import { BleManager } from 'react-native-ble-plx';
import BLEPeripheral from 'react-native-ble-peripheral';
import Global from "./global";

const bleManager = new BleManager();
var timestampStart = 0;
var timestampEnd = 0;
var listDevice = {};
var newRecords = [];

export const isBluetoothActivated = async() => {
	try {
		const state = await bleManager.state();
		console.log("Bluetooth state: " + state);
		if(state === "PoweredOn") {
			return true;
		} else {
			return false;
		}
	} catch(e) {
		console.log("Bluetooth error: " + e);
		return false;
	}
}

export const startAdvertising = async() => {
	console.log("startAdvertising ...");

	BLEPeripheral.addService(Global.serviceUUID, false);

	const idClientString = Global.idClient.toString();
	BLEPeripheral.setName(idClientString);
	BLEPeripheral.start().then(res => {
			 console.log(res);
	}).catch(error => {
			 console.log(error);
	});
}

export const stopAdvertising = async() => {
  console.log("stopAdvertising ...");
  BLEPeripheral.stop();
}

export const activateScan = async(scanDuration) => {
	timestampStart = Math.floor(Date.now() / 1000);
	console.log("Start scanning !!!! ---");

	bleManager.startDeviceScan(null, null, (error, device) => {
		if((Date.now() / 1000) - timestampStart > scanDuration) {
			console.log("Elapsed time : ", (Date.now() / 1000) - timestampStart);
			desactivateScan();
		}

		if(error) {
			console.log("Bluetooth scan error: " + error.message);
		} else {

			if(device.serviceUUIDs !== null && device.serviceUUIDs.length === 1) {
			 	if(device.serviceUUIDs[0] === Global.serviceUUID) {
					//console.log("Device found. Name : " + device.localName)

					if(!(device.localName in listDevice)) {
						listDevice[device.localName] = [];
					}

					listDevice[device.localName].push([device.rssi, Date.now() / 1000]);
					//listDevice[device.localName].push([device.rssi]);
				}
			}
		}
	});
}

export const desactivateScan = async() => {
	timestampEnd = Math.floor(Date.now() / 1000);
	console.log("Stop scanning !!!! ---");
	bleManager.stopDeviceScan();
	buildProximityRecords();
}

function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}

function meanList(list) {
  let total = 0, i;
  for(i = 0; i < list.length; i++)
  {
    total += list[i];
  }
  return round(total / list.length, 2);
}

function computeDistance(rssi) {
	const txPower = -59;
	const envFactor = 2;
	return Math.pow(10, (txPower - rssi) / (10 * envFactor));
}

function modeRSSI(listRSSI) {
	let modes = [], count = [], i, rssi, maxIndex = 0;

	for(i = 0; i < listRSSI.length; i++)
	{
	   rssi = listRSSI[i];
	   count[rssi] = (count[rssi] || 0) + 1;
	   if(count[rssi] > maxIndex)
	   {
	   	maxIndex = count[rssi];
	   }
	}

	for(i in count)
	{
	 	if(count.hasOwnProperty(i))
	  {
	   	if(count[i] === maxIndex)
	    {
	    	modes.push(Number(i));
	    }
	  }
	}

	// As it can be bimodal or multi-modal the returned result is provided as an array
	return modes;
}

export const getNewRecords = () => {
	return newRecords;
}

async function buildProximityRecords() {
	let idTarget, nbDetection, avgRSSI, avgDistance;
	newRecords = [];

	for(idTarget in listDevice) {
		nbDetection = listDevice[idTarget].length;
		avgRSSI = meanList(modeRSSI(listDevice[idTarget]));
		avgDistance = computeDistance(avgRSSI);
		//avgRSSI = listDevice[name].reduce((previous, current) => current += previous) / nbDetection;

		//newRecords.push([idTarget, timestampStart, timestampEnd, avgRSSI, avgDistance, nbDetection]);
		for(let i=0; i<listDevice[idTarget].length; i++) {
		 	newRecords.push([idTarget, listDevice[idTarget][i][1], 0, listDevice[idTarget][i][0], computeDistance(listDevice[idTarget][i][0]), 1]);
		}
	}

	listDevice = {};
}
