import AsyncStorage from '@react-native-community/async-storage';

const RECORDS_KEY = "@records";
const IDCLIENT_KEY = "@idclient";

const getIdClientInStorage = async() => {
  try {
    const idClient = await AsyncStorage.getItem(IDCLIENT_KEY);
    return idClient;
  } catch(e) {
    console.log("Error AsyncStorage: " + e);
    return null;
  }
}

const setIdClientInStorage = async(idClient) => {
  try {
    console.log("Store:" + JSON.stringify(idClient));
    await AsyncStorage.setItem(IDCLIENT_KEY, JSON.stringify(idClient));

  } catch(e) {
    console.log("Error AsyncStorage: " + e);
  }
}

const getRecordsInStorage = async() => {
  try {
    const storeRecords = await AsyncStorage.getItem(RECORDS_KEY);
    return JSON.parse(storeRecords);
  } catch(e) {
    console.log("Error AsyncStorage: " + e);
    return null;
  }
}

const storeRecordInStorage = async(records) => {
  try {
    const storeRecords = await getRecordsInStorage();
    await removeRecordsInStorage();
    let save_records = [];
    console.log("storerecords: ");
    console.log(storeRecords);
    if(storeRecords !== null) {
      save_records = storeRecords;
    }

    for(let i=0; i<records.length; i++) {
      save_records.push(records[i]);
    }

    await AsyncStorage.setItem(RECORDS_KEY, JSON.stringify(save_records));

  } catch(e) {
    console.log("Error AsyncStorage: " + e);
  }
}

const removeRecordsInStorage = async() => {
  try {
    await AsyncStorage.removeItem(RECORDS_KEY);
  } catch(e) {
    console.log("Error AsyncStorage: " + e);
  }
}

export {
  getIdClientInStorage,
  setIdClientInStorage,
  getRecordsInStorage,
  storeRecordInStorage,
  removeRecordsInStorage
}
