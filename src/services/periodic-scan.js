import BackgroundFetch from "react-native-background-fetch";
import { activateScan, desactivateScan, startAdvertising, stopAdvertising, getNewRecords, isBluetoothActivated } from "./bluetooth-scan";
import { getRecordsInStorage, removeRecordsInStorage } from './local-storage';
import Global from './global';

var records = [];

export const stopBackgroundRecordingTask = async() => {
  Global.isStarted = false;
  await desactivateScan();

  if(Global.isAdvertising) {
    Global.isAdvertising = false;
    await stopAdvertising();
  }

	await Global.updateScreen();
  BackgroundFetch.stop();
}

export const setBackgroundRecordingTask = async() => {
  BackgroundFetch.configure({
    minimumFetchInterval: 5,     // <-- minutes (15 is minimum allowed)
    // Android options
    stopOnTerminate: false,
    startOnBoot: true,
    requiredNetworkType: BackgroundFetch.NETWORK_TYPE_NONE, // Default
    requiresCharging: false,      // Default
    requiresDeviceIdle: false,    // Default
    requiresBatteryNotLow: false, // Default
    requiresStorageNotLow: false  // Default
  }, async(taskId) => {
    console.log(Global.globalTimeTask);
    Global.globalTimeTask = Date.now() / 1000;
    
		await Global.queue.start((Global.scanDuration * 1000) + 5000);

		storeRecords = await getRecordsInStorage();
		records = storeRecords;
		console.log("Records to send:");
		console.log(records);

		if(records !== null) {
			console.log("Sending records -----------------");
			Global.queue.createJob("send-record", { records }, { attempts: 1, timeout: 500 }, false);
			await removeRecordsInStorage();
		}

		if(await Global.requestPermissions() && await isBluetoothActivated()) {
			if(!Global.isAdvertising) {
        await startAdvertising();
        Global.isAdvertising = true;
      }

      activateScan(Global.scanDuration);
      records = getNewRecords();
			console.log("Nb scan: " + records.length);

      if(records.length > 0) {
        Global.queue.createJob("send-record", { records }, { attempts: 1, timeout: 500 }, false);
      }

      BackgroundFetch.finish(taskId);
    } else {
      await stopBackgroundRecordingTask();
    }

  }, (error) => {
    console.log("[js] RNBackgroundFetch failed to start");
  });
  BackgroundFetch.status((status) => {
    switch(status) {
      case BackgroundFetch.STATUS_RESTRICTED:
        console.log("BackgroundFetch restricted");
        break;
      case BackgroundFetch.STATUS_DENIED:
        console.log("BackgroundFetch denied");
        break;
      case BackgroundFetch.STATUS_AVAILABLE:
        console.log("BackgroundFetch is enabled");
        break;
    }
  });
}
