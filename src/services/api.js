import { AsyncStorage } from 'react-native';
import axios from 'axios';

const API = "http://igem-ties.info:7760";

const axiosAPI = axios.create({
  baseURL: API,
  timeout: 5000,
});

export const api = async (uri, method, data = null) => {
  const result = await axiosAPI({
    url: `api/${uri}`,
    method,
    headers: {},
    data
  }).catch(() => { return null; });

  return result;
}

export const getIdClientAPI = async() => {
  return api("id", "GET");
}

export const sendRecordsAPI = async(idSource, records) => {
  return await api("ping", "POST", {
    idSource,
    records
  });
}
