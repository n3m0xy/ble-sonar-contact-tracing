import { PermissionsAndroid, Platform, Alert } from "react-native";
import { AsyncStorage } from 'react-native';
import { Component } from 'react';
import queueFactory from 'react-native-queue';
import { getIdClientInStorage, setIdClientInStorage, storeRecordInStorage, removeRecordsInStorage } from './local-storage';
import { setBackgroundRecordingTask, stopBackgroundRecordingTask } from './periodic-scan';
import { getIdClientAPI, sendRecordsAPI } from './api';
import { isBluetoothActivated } from './bluetooth-scan';

class Global {
  constructor() {
    this.globalTimeTask = Date.now() / 1000;
    this.serviceUUID = "cca2e4c3-de6b-45cc-85dc-ced34ae3ddef";
    this.characteristicUUID = "0000180a-0000-1475-8980-00805f9b34fb";

    this.MainScreen = null;
    this.idClient = null;

    // Duration of a scan in seconds
    this.scanDuration = 40;
    this.isStarted = false;
    this.isAdvertising = false;
  }

  init = async(MainScreen) => {
    this.MainScreen = MainScreen;
    this.idClient = await getIdClientInStorage();
    if(this.idClient === undefined || this.idClient === null) {
      result = await getIdClientAPI();

      if(result === null) {
        console.log("Error API");
      } else {
        this.idClient = result.data["idClient"];
        await removeRecordsInStorage();
        await setIdClientInStorage(this.idClient);
      }
    }

    console.log("idclient: " + this.idClient);
    this.MainScreen.update(this.idClient, this.isStarted);

    this.queue = await queueFactory();
    this.queue.addWorker("send-record", async (id, payload) => {
      await this.sendRecords(payload.records);
    });
  }

  Flush = async() => {
    console.log("Flush data");
    await removeRecordsInStorage();
  }

  BLEmanager = async(scanDuration) => {
    if(!this.isStarted) {
      this.scanDuration = scanDuration;
      console.log("Scan duration: " + this.scanDuration);
      if(await this.requestPermissions() && await isBluetoothActivated()) {
        setBackgroundRecordingTask();
        this.isStarted = true;
        await this.updateScreen();
      } else {
        Alert.alert("Notification", "You need to 'enable Bluetooth' and 'authorize location' to use this application.");
      }
    } else {
      await stopBackgroundRecordingTask();
    }
  }

  requestPermissions = async() => {
  	if (Platform.OS === 'android') {
  		try {
  			const grantedCoarseLocation = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
  			if (grantedCoarseLocation === PermissionsAndroid.RESULTS.GRANTED) {
  				console.log('Access Fine Location');
          return true;
  			}

  			return false;

  		} catch(e) {
  			return false;
  		}
  	}
  	else { return true; }
  }

  sendRecords = async(records) => {
    console.log("Sending :");
    console.log(records);
    let req = await sendRecordsAPI(this.idClient, records);
    if(req === null) {
      console.log("Error sending --");
      await storeRecordInStorage(records);
    }

    console.log("Nb sent: " + records.length);
  }

  updateScreen = async() => {
    this.MainScreen.update(this.idClient, this.isStarted);
  }
}

export default new Global();
