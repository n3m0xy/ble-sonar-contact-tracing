import { createStackNavigator } from 'react-navigation-stack';

import MainScreen from '../screens/MainScreen';

let HomeStack = createStackNavigator(
  {
    Home: MainScreen,
  },
  {
    headerMode: 'none',
  },
);

HomeStack.navigationOptions = {
  headerVisible: true,
};

export default HomeStack;
