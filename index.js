/**
 * @format
 */

import {AppRegistry} from 'react-native';
import React, {useState, useEffect} from 'react';
import App from './src/App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
